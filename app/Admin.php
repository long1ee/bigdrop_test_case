<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $table = 'admin';

    protected $fillable = [
        'id',
        'fist_name',
        'last_name',
        'email',
        'token',
    ];

    protected $visible = [
        'id',
        'fist_name',
        'last_name',
        'email',
        'token',
    ];
}
