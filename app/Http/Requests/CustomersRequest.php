<?php

namespace App\Http\Requests;

class CustomersRequest extends ApiRequest
{

    public function rules()
    {
        return [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|email|string|unique:customers,email' . $this->id,
            'token' => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',

        ];
    }
}
