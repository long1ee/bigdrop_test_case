<?php

namespace App\Http\Controllers;

use App\Admin;

class AdminController extends ApiController
{
    /**
     * AdminController constructor.
     * @param Admin $model
     */
    public function __construct(Admin $model)
    {
        $this->model = $model;
    }
}
