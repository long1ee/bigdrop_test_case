<?php

namespace App\Http\Controllers;

use App\Customers;
use App\Http\Requests\ApiRequest;
use App\Http\Requests\CustomersRequest;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Tests\Feature\CustomersTest;


/**
 * Class ApiController
 * @package App\Http\Controllers
 */
class ApiController extends Controller
{

    /**
     * @var
     */
    public $model;
    public $request;

    /**
     * @param Request $request
     * @return mixed
     */
    public function someUrl(Request $request) {
        $message = 'ERROR';
        $token = $request->get('token');
        if (!$token) {
            return $this->sendError($message, 200, ['token' => 'Token is required']);
        }

        $customer = $this->model->where('token', '=', $token)->first();
        if ($customer) {
            if ($customer->status == Customers::STATUS_APPROVED_YES) {
                $message = 'OK';
                $result['message'] = 'Hello ' . $customer->first_name . ' ' . $customer->last_name;
            } else {
                $result = [
                    'status' => $customer->status,
                    'message' => ' You not have permissions fo this url',
                ];
            }
        } else {
            $result = ['user' => 'Customer not found'];
        }
        return $this->sendResponse($result, $message,200);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getToken(Request $request)
    {
        $message = 'ERROR';
        $email = $request->get('email');
        if (!$email) {
            return $this->sendError($message, 200, ['token' => 'Token is required']);
        }
        $existUser = $this->model->where('email', '=', $email)->first();
        if ($existUser) {
            $message = 'OK';
            $existUser->token = Str::random(32);
            $existUser->save();
            $result = ['token' => $existUser->token];
        } else {
            $result = ['user' => 'User not found'];
        }
        return $this->sendResponse($result, $message, 200);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function list(Request $request)
    {
        $message = 'ERROR';

        $token = $request->get('token');
        if (!$token) {
            return $this->sendError($message, 200, ['token' => 'Token is required']);
        }
        $admin = $this->model->where('token', '=', $token)->first();
        if ($admin) {
            $message = 'OK';
            $result['customers'] = [];
            $limit = (int) $request->get('limit', 10);
            $offset = (int) $request->get('offset', 0);
            $customersData = Customers::find()->limit($limit)->offset($offset)->get();
//            $customersData = DB::table('customers')->limit($limit)->offset($offset)->get();
            if ($customersData) {
                foreach ($customersData as $item) {
                    $data[] = [
                        'id' => $item->id,
                        'email' => $item->email,
                        'first_name' => $item->first_name,
                        'last_name' => $item->last_name,
                        'city' => $item->city,
                    ];
                }
                $result['customers'] = $data;
            }
        } else {
            $result = ['admin' => 'Admin not found'];
        }
        return $this->sendResponse($result, $message,200);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function approve(Request $request)
    {
        $message = 'ERROR';

        $token = $request->post('token');
        if (!$token) {
            return $this->sendError($message, 200, ['token' => 'Token is required']);
        }

        $customerId = $request->post('customer_id');
        if (!$customerId) {
            return $this->sendError($message, 200, ['customer_id' => 'Customer Id is required']);
        }

        $admin = $this->model->where('token', '=', $token)->first();
        if ($admin) {
            $customerData = Customers::find($customerId);
            if ($customerData) {
                $customerData->status = Customers::STATUS_APPROVED_YES;
                if ($customerData->save()) {
                    $message = 'OK';
                    $result = [
                      'customer_id' => $customerData->id,
                      'customer_email' => $customerData->email,
                      'customer_status' => $customerData->status,
                    ];
                }
            }
        } else {
            $result = ['admin' => 'Admin not found'];
        }
        return $this->sendResponse($result, $message,200);

    }

    public function create(Request $request)
    {
        $request->request->add(['token' => Str::random(32)]);
        $validator = Validator::make($request->request->all(), (new CustomersRequest())->rules());

        if ($validator->fails()) {
            return $this->sendError('ERROR', 200, $validator->errors()->toArray());
        } else {
            $this->model->fill($request->request->all())->push();
            return $this->sendResponse($this->model, 'Created', 200);
        }
    }
}