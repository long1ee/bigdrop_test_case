<?php

namespace App\Http\Controllers;


use App\Customers;
use App\Http\Requests\CustomersRequest;

class CustomerController extends ApiController
{
    /**
     * CustomerController constructor.
     * @param Customers $model
     */
    public function __construct(Customers $model)
    {
        $this->model = $model;
    }

    /**
     * @param CustomersRequest $request
     * @return mixed
     */
    public function createCustomer(CustomersRequest $request)
    {
        return $this->create($request);
    }
}
