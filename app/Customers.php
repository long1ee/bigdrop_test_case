<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{

    const STATUS_APPROVED_NO = 0;
    const STATUS_APPROVED_YES = 1;


    protected $table = 'customers';

    protected $rules = [
        'email' => 'required|email|unique:customers',
    ];

    protected $fillable = [
        'id',
        'first_name',
        'last_name',
        'email',
        'city',
        'state',
        'token',
    ];

    protected $visible = [
        'first_name',
        'last_name',
        'email',
        'city',
        'token',
    ];
}
