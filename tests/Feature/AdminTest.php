<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public $testCreateAdminData = [
        'first_name' => 'artem',
        'last_name' => 's',
        'email' => 'admin@gmai.com',
    ];

    public function testGetToken()
    {
        $response = $this->post('/api/customers/token',[
            'email' => $this->testCreateAdminData['email'],
        ]);
        $response->assertStatus(200);
    }

    public function testGetCustomersList()
    {
        $response = $this->post('/api/customers/token',[
            'email' => $this->testCreateAdminData['email'],
        ]);
        $result = json_decode($response->getContent(), true);
        $token = $result['data']['token'];

        $response = $this->call('GET', '/api/admin/list',[
            'token' => $token,
        ]);
        $response->assertStatus(200);
    }

    public function testUpdateCustomerStatus()
    {
        $response = $this->post('/api/customers/token',[
            'email' => $this->testCreateAdminData['email'],
        ]);
        $result = json_decode($response->getContent(), true);
        $token = $result['data']['token'];

        $response = $this->call('GET', '/api/admin/list',[
            'token' => $token,
        ]);
        $result = $result = json_decode($response->getContent(), true);
        $customerList = $result['data']['customers'];
        if (!empty($customerList)) {
            $customer = current($customerList);
        }

        $response = $this->post('/api/admin/approve',[
            'token' => $token,
            'customer_id' => $customer['id'],
        ]);

        $response->assertStatus(200);
    }
}
