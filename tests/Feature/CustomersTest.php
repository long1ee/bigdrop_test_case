<?php

namespace Tests\Feature;

use Psy\Util\Json;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CustomersTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public $token;
    public $testCreateCustomerData = [
        'first_name' => 'artem',
        'last_name' => 's',
        'email' => 'a@gmai.com',
    ];


    public function testCreateCustomer()
    {
        $response = $this->post('/api/customers/create',$this->testCreateCustomerData);
        /*echo "<pre>";
        print_r($response->getContent());
        die('STOP TEST');*/
        $response->assertStatus(200);
    }


    public function testGetToken()
    {
        $response = $this->post('/api/customers/token',[
            'email' => $this->testCreateCustomerData['email'],
        ]);
        $response->assertStatus(200);
    }

    public function testGetCustomerUrl()
    {
        $response = $this->post('/api/customers/token',[
            'email' => $this->testCreateCustomerData['email'],
        ]);
        $result = json_decode($response->getContent(), true);
        $token = $result['data']['token'];

        $response = $this->post('/api/customers/someurl',[
            'token' => $token,

        ]);
        $response->assertStatus(200);
    }
}
