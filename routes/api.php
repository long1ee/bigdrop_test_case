<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/customers')->group(function () {

    Route::post('/someurl', ['uses' => 'CustomerController@someUrl']);
    Route::post('/create', ['uses' => 'CustomerController@createCustomer']);
    Route::post('/token', ['uses' => 'CustomerController@getToken']);
});


Route::prefix('/admin')->group(function () {

    Route::post('/approve', ['uses' => 'AdminController@approve']);
    Route::get('/list', ['uses' => 'AdminController@list']);
    Route::post('/token', ['uses' => 'AdminController@getToken']);
});