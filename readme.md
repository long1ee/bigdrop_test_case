Test case 
1.  Существует 2 сущности пользователей. Admin, Customer . 
Список обязательных полей :
a. Admin
i. Email
ii. First Name
iii. Last Name
b. Customer
i. Email
ii. First Name
iii. Last Name
iv. City
v. State
2. Customer отправляет запрос на регистрацию. После подтверждения Admin’(ом), customer может авторизоваться.
3. Авторизованным customer’(ам) доступен endpoint, который возвращает 
“Hello {FirstName} {LastName}!”

Используя framework Laravel или Lumen создать API, который будет реализовывать данный функционал. Для удобства можно опубликовать на github.

Большим плюсом будут тесты, которые покроют данный функционал.